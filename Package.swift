// swift-tools-version:5.5

import PackageDescription

let package = Package(
    name: "UJET",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "UJET",
            targets: ["UJETSDK"])
    ],
    dependencies: [
         .package(url: "https://github.com/twilio/conversations-ios", from: "1.3.1"),
         .package(url: "https://github.com/twilio/twilio-voice-ios", from: "6.3.0")
    ],
    targets: [
        .binaryTarget(
            name: "UJET",
            url: "https://ujet-jaesung.s3.us-west-2.amazonaws.com/ios/0.43.0/UJET.xcframework.zip",
            checksum: "83776c7301fcbf60f13f726847edf047fb7444c9d4e992f719ec6b8f5212c6b2"),

        .target(
            name: "UJETSDK",
            dependencies: [
                "UJET", 
                .product(name: "TwilioConversationsClient", package: "conversations-ios"), 
                .product(name: "TwilioVoice", package: "twilio-voice-ios")
            ]
        )
    ]
)
